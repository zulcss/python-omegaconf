#!/usr/bin/make -f

UPSTREAM_GIT := https://github.com/omry/omegaconf.git
include /usr/share/openstack-pkg-tools/pkgos.make

export http_proxy=127.0.0.1:9
export https_proxy=127.0.0.1:9
export HTTP_PROXY=127.0.0.1:9
export HTTPS_PROXY=127.0.0.1:9

%:
	dh $@ --buildsystem=python_distutils --with python3

override_dh_auto_clean:
	rm -rf build .stestr
	find . -iname '*.pyc' -delete
	for i in $$(find . -type d -iname __pycache__) ; do rm -rf $$i ; done

override_dh_auto_build:
	echo "Do nothing..."

override_dh_auto_install:
	pkgos-dh_auto_install --no-py2 --in-tmp
	# Replace build path with a placeholder string for reproducible builds
	sed -i -e "s,$(CURDIR),BUILDPATH,g" \
		debian/tmp/usr/lib/python3/dist-packages/omegaconf/grammar/gen/OmegaConfGrammarParserVisitor.py \
		debian/tmp/usr/lib/python3/dist-packages/omegaconf/grammar/gen/OmegaConfGrammarParserListener.py \
		debian/tmp/usr/lib/python3/dist-packages/omegaconf/grammar/gen/OmegaConfGrammarParser.py \
		debian/tmp/usr/lib/python3/dist-packages/omegaconf/grammar/gen/OmegaConfGrammarLexer.py \

ifeq (,$(findstring nocheck, $(DEB_BUILD_OPTIONS)))
	for i in `py3versions -rv` ; do \
		python$$i -m pytest tests -v --disable-warnings -p no:warnings ; \
	done
endif

override_dh_install:
	dh_install
	rm $(CURDIR)/debian/python3-omegaconf/usr/lib/python3/dist-packages/pydevd_plugins/__init__.py
	rm $(CURDIR)/debian/python3-omegaconf/usr/lib/python3/dist-packages/pydevd_plugins/extensions/__init__.py

override_dh_auto_test:
	echo "Do nothing..."
